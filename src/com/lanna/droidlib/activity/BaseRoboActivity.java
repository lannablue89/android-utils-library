package com.lanna.droidlib.activity;

import roboguice.activity.RoboFragmentActivity;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;

import com.lanna.droidlib.utils.UiUtils;

public class BaseRoboActivity extends RoboFragmentActivity implements OnClickListener {
	
	protected Context mContext;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mContext = this;
	}
	
	@Override
	public void onClick(View v) {
		UiUtils.hideKeyboard(mContext, v);
	}

}
