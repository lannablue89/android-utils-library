package com.lanna.droidlib.view;

import java.util.ArrayList;
import java.util.List;

import com.lanna.droidlib.utils.Utils;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

public abstract class BaseAdapter2<T> extends BaseAdapter {
	String tag = BaseAdapter2.class.getSimpleName();

	private Context mContext;
	protected LayoutInflater mInflater;
	private boolean isAllowNullModelOnViewItem;
	
	private boolean mIsReuseConvertView;
	private List<View> mViews;

	protected List<T> mModels;
	
	public BaseAdapter2(Context context, boolean isReuseConvertView) {
		mContext = context;
		mInflater = LayoutInflater.from(context);
		setIsReuseConvertView(isReuseConvertView);
	}

	public BaseAdapter2(Context context) {
		this(context, true);
	}

	public BaseAdapter2(Context context, List<T> models, boolean isReuseConvertView) {
		this(context, isReuseConvertView);
		mModels = models;
	}

	public BaseAdapter2(Context context, List<T> models) {
		this(context);
		mModels = models;
	}

	public abstract IViewHolder<T> getViewHolder(int position);

	public List<T> getModels() {
		return mModels;
	}

	public void setModelsAndNotify(List<T> models) {
		setModels(models);
		notifyDataSetChanged();
	}

	public void setModels(List<T> models) {
		mModels = models;
	}

	@Override
	public int getCount() {
		return mModels == null ? 0 : mModels.size();
	}

	@Override
	public Object getItem(int position) {
		return Utils.isPositionAvailable(mModels, position) ? mModels.get(position) : null;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@SuppressWarnings("unchecked")
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
//		Log.i(tag, "getView position="+position);
		IViewHolder<T> holder = null;

		// case 1: Not reuse convertView
		if (!mIsReuseConvertView) {
			// always assign new view
			if (position >= mViews.size()) {
				holder = getViewHolder(position);
				convertView = mInflater.inflate(holder.getLayoutId(), null);
				holder.initView(convertView, position);
				convertView.setTag(holder);
//				Log.d("lanna", "new convertview " + position);
				mViews.add(convertView);
			} else {
				convertView = mViews.get(position);
				holder = (IViewHolder<T>) convertView.getTag();
//				Log.d("lanna", "reused convertview " + position);
			}
		}

		// case 2: reuse convertView
		// In this case, icons between rows different may be in changed many times
		else {
//			Log.d("lanna", "normal convertview " + position);
			// do as normal/common behavior
			if (convertView == null) {
				holder = getViewHolder(position);
				convertView = mInflater.inflate(holder.getLayoutId(), null);
				holder.initView(convertView, position);
				convertView.setTag(holder);
			} else {
				holder = (IViewHolder<T>) convertView.getTag();
			}
		}

//		Log.i(tag, "isAllowNullModelOnViewItem=" + isAllowNullModelOnViewItem + ", holder=" + holder);
		if (holder != null) {
			holder.resetView(position);

			T model = (T) getItem(position);
//			Log.i(tag, "model=" + model);
			if (isAllowNullModelOnViewItem || (!isAllowNullModelOnViewItem && model != null)) {
				holder.initData(model, position);
			}
		}
		return convertView;
	}
	
	/*
	 * Getters, Setters
	 */

	protected Context getContext() {
		return mContext;
	}
	
	protected boolean isReuseConvertView() {
		return mIsReuseConvertView;
	}

	protected void setIsReuseConvertView(boolean isReuseConvertView) {
		this.mIsReuseConvertView = isReuseConvertView;
		if (!mIsReuseConvertView && mViews == null) {
			mViews = new ArrayList<View>();
		}
		notifyDataSetChanged();
	}

	protected boolean isAllowNullModelOnViewItem() {
		return isAllowNullModelOnViewItem;
	}

	protected void setAllowNullModelOnViewItem(boolean isAllowNullModelOnViewItem) {
		this.isAllowNullModelOnViewItem = isAllowNullModelOnViewItem;
	}

	public List<View> getUnReusedItemViews() {
		return mViews;
	}

}
