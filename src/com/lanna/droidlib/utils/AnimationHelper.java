package com.lanna.droidlib.utils;

import android.animation.Animator.AnimatorListener;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Interpolator;
import android.widget.ProgressBar;
import android.widget.TextView;


public class AnimationHelper {

	public static final int DEFAULT_DURATION = 350;
	public static final int DEFAULT_DURATION_TEXT_UNIT = 50;
	
	public static final String ALPHA = "alpha";
	public static final String TRANSLATION_Y = "translationY";
	public static final String TRANSLATION_X = "translationX";
	public static final String SCALE_X = "scaleX";
	public static final String SCALE_Y = "scaleY";
	public static final String WIDTH = "width";
	public static final String HEIGHT = "height";
	
	public static void startAnimations(int duration, int delay, ObjectAnimator... animators) {
		AnimatorSet animSet = new AnimatorSet();
		animSet.playTogether(animators);
		animSet.setDuration(duration);
		animSet.setStartDelay(delay);
		animSet.start();
	}
	
	public static void startAnimation(ValueAnimator anim, AnimatorListener animListener, int duration, int delay) {
		if (animListener != null) {
			anim.addListener(animListener);
		}
		startAnimation(anim, duration, delay);
	}
	
	public static void startAnimation(ValueAnimator anim, int duration, int delay) {
		anim.setDuration(duration);
		anim.setStartDelay(delay);
		anim.start();
	}

	public static void showViewTopDownBelongItsHeight(View v, int duration, int delay) {
		startAnimations(duration, delay,
				ObjectAnimator.ofFloat(v, ALPHA, 0.0f, 1.0f),
				ObjectAnimator.ofFloat(v, TRANSLATION_Y, - v.getHeight(), 0.0f)
				);
	}

	public static void hideViewTopDownBelongItsHeight(TextView v, int duration, int delay) {
		startAnimations(duration, delay,
				ObjectAnimator.ofFloat(v, ALPHA, 1.0f, 0.0f),
				ObjectAnimator.ofFloat(v, TRANSLATION_Y, 0, v.getHeight())
				);
	}

	public static void scaleViewX(View v, float rateWidth, int duration, int delay, AnimatorListener animListener) {
		startAnimation(ObjectAnimator.ofFloat(v, SCALE_X, rateWidth), animListener, duration, delay);
	}

	public static void scaleViewX(View v, float rateWidth, int duration, int delay) {
		startAnimation(ObjectAnimator.ofFloat(v, SCALE_X, rateWidth), duration, delay);
	}
	
	public static void scaleViewWidth(final View v, float width, int duration, int delay, final Runnable onUpdateAnim) {
		if (width == 0) {
			return;
		}
		
		Log.d("lanna", "scaleViewWidth w=" + v.getWidth() + " to width=" + width);
//		ValueAnimator anim = ValueAnimator.ofInt(v.getMeasuredWidth(), width);
		if (v.getWidth() == 0) {
            ViewGroup.LayoutParams layoutParams = v.getLayoutParams();
            layoutParams.width = 1; // min width
            v.setLayoutParams(layoutParams);
		}
		float rateWidth = v.getWidth() == 0 ? width : width / v.getWidth();
		v.setPivotX(0);
		ObjectAnimator anim = ObjectAnimator.ofFloat(v, SCALE_X, 1, rateWidth);
	    anim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
	        @Override
	        public void onAnimationUpdate(ValueAnimator valueAnimator) {
	            float rateWidth = (Float) valueAnimator.getAnimatedValue();
				Log.i("lanna", "onAnimationUpdate w=" + v.getWidth() + " to anim rateWidth=" + rateWidth);
	            if (onUpdateAnim != null) {
	            	onUpdateAnim.run();
	            }
	        }
	    });
	    startAnimation(anim, duration, delay);
	}
	
	public static void animProgress(final ProgressBar progressBar, final int progressStart, final int progressEnd, 
			int duration, int delay, final AnimatorListener animListener, final Runnable onUpdateAnim) {
		progressBar.setProgress(progressStart);
		ValueAnimator anim = ValueAnimator.ofInt(progressStart, progressEnd); 
		anim.setDuration(duration);
		anim.setStartDelay(delay);
		if (animListener != null) {
			anim.addListener(animListener);
		}
		anim.addUpdateListener(new AnimatorUpdateListener() {

		        @Override
		        public void onAnimationUpdate(ValueAnimator animation) {
		            int progressEnd = (Integer) animation.getAnimatedValue();
		            progressBar.setProgress(progressEnd);
		            if (onUpdateAnim != null) {
		            	onUpdateAnim.run();
		            }
		        }
		    });
		anim.start();
	}

	public static void startAnimationTextChanged(final TextView tv,
			float from, float to, final String format, int duration) {
		
		ValueAnimator textAnimator = new ValueAnimator();
		textAnimator.setObjectValues(from, to);
		textAnimator.addUpdateListener(new AnimatorUpdateListener() {
		    public void onAnimationUpdate(ValueAnimator animation) {
		    	String text = String.format(format, (Float) animation.getAnimatedValue());
		    	tv.setText(text);
		    }
		});
		duration = Math.min((int)((to-from)*AnimationHelper.DEFAULT_DURATION_TEXT_UNIT), duration);
		textAnimator.setDuration(duration);
		textAnimator.start();
	}

	public static void startAnimationTextChangedAsInt(TextView tv,
			int from, int to, String format) {
		startAnimationTextChanged(tv, from, to, format, 10*AnimationHelper.DEFAULT_DURATION_TEXT_UNIT);
	}

	public static ObjectAnimator getSetOfAnimation(View v, int duration, Interpolator interpolator, PropertyValuesHolder... values) {
        ObjectAnimator yAlphaBouncer = ObjectAnimator.ofPropertyValuesHolder(v, values).setDuration(duration);
        yAlphaBouncer.setInterpolator(interpolator);
        yAlphaBouncer.setRepeatCount(1);
        yAlphaBouncer.setRepeatMode(ValueAnimator.REVERSE);
        return yAlphaBouncer;
	}
	public static ObjectAnimator getSetOfAnimation(View v, int duration, PropertyValuesHolder... values) {
        ObjectAnimator yAlphaBouncer = ObjectAnimator.ofPropertyValuesHolder(v, values).setDuration(duration);
        return yAlphaBouncer;
	}

}
