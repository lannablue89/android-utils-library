package com.lanna.droidlib.utils;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.NinePatchDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.os.SystemClock;
import android.provider.CalendarContract;
import android.provider.CalendarContract.Events;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.lanna.droidlib.androidutils.R;

public class UiUtils {
	static String tag = UiUtils.class.getSimpleName();

	/**
	 * This method convets dp unit to equivalent device specific value in
	 * pixels.
	 *
	 * @param dp
	 *            A value in dp(Device independent pixels) unit. Which we need
	 *            to convert into pixels
	 * @param context
	 *            Context to get resources and device specific display metrics
	 * @return A float value to represent Pixels equivalent to dp according to
	 *         device
	 */
	public static float convertDpToPixel(Context context, float dp) {
		Resources resources = context.getResources();
		DisplayMetrics metrics = resources.getDisplayMetrics();
		float px = dp * (metrics.densityDpi / 160f);
		return px;
	}

	/**
	 * This method converts device specific pixels to device independent pixels.
	 *
	 * @param px
	 *            A value in px (pixels) unit. Which we need to convert into db
	 * @param context
	 *            Context to get resources and device specific display metrics
	 * @return A float value to represent db equivalent to px value
	 */
	public static float convertPixelsToDp(Context context, float px) {
		Resources resources = context.getResources();
		DisplayMetrics metrics = resources.getDisplayMetrics();
		float dp = px / (metrics.densityDpi / 160f);
		return dp;
	}

	public static void startNewActivity(Activity actThis,
			Class<?> actNew, boolean... isFinishThis) {
		Intent intent = new Intent(actThis, actNew);
		if (Utils.hasData(isFinishThis) && isFinishThis[0] == true)
			actThis.finish();
		actThis.startActivity(intent);
	}

	public static void startNewActivity(Activity actThis,
			Class<?> actNew, String key, Parcelable data, boolean... isFinishThis) {
		Intent intent = new Intent(actThis, actNew);

		// set data
		Bundle b = new Bundle();
		b.putParcelable(key, data);
		intent.putExtras(b);

		if (Utils.hasData(isFinishThis) && isFinishThis[0] == true)
			actThis.finish();

		actThis.startActivity(intent);
	}

	public static void showKeyboard(Context context, EditText editText) {
		try {
			editText.requestFocus();
			editText.dispatchTouchEvent(MotionEvent.obtain(
					SystemClock.uptimeMillis(), SystemClock.uptimeMillis(),
					MotionEvent.ACTION_DOWN, 0, 0, 0));
			editText.dispatchTouchEvent(MotionEvent.obtain(
					SystemClock.uptimeMillis(), SystemClock.uptimeMillis(),
					MotionEvent.ACTION_UP, 0, 0, 0));
		} catch (NullPointerException e) {
			e.printStackTrace();
		}
	}
	
	public static void popupAlertQuitApp(final Context context) {
		popAlert2Choose(context, null, "Quit app?", "OK", new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				((Activity) context).finish();
			}
		}, "Cancel", null);
	}

	public static void hideKeyboard(Context context, View v) {
		InputMethodManager imm = (InputMethodManager) context
				.getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
//		imm.hideSoftInputFromWindow(((Activity)context).getCurrentFocus().getWindowToken(), 0);
	}

	public static void popupAlertMessage(Context context, int title,
			int message, int btnPositiveString,
			final Runnable... okPressedRunnable) {
		AlertDialog.Builder alert = new AlertDialog.Builder(context);
		if (title > 0)
			alert.setTitle(title);
		if (message > 0)
			alert.setMessage(message);
		if (btnPositiveString > 0)
			alert.setPositiveButton(btnPositiveString,
					new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog,
								int which) {
							if (Utils.hasData(okPressedRunnable)) {
								okPressedRunnable[0].run();
							}
						}
					});
		alert.create().show();
	}

	public static void popupAlertMessage(Context context, String title,
			String message, String btnPositiveString, final Runnable... okPressedRunnable) {
		new AlertDialog.Builder(context)
		.setTitle(title)
		.setMessage(message)
		.setPositiveButton(btnPositiveString,
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog,
							int which) {
						if (Utils.hasData(okPressedRunnable)) {
							okPressedRunnable[0].run();
						}
					}
				}).create().show();
	}

	public static void makeShortToast(Context context, String content) {
		customToast(context, content);
	}

	public static void makeShortToast(Context context, int content) {
		customToast(context, context.getResources().getString(content));
	}

	public static void makeDebugToast(Context context, String content) {
		if (Log.isDebug) {
			makeShortToast(context, content);
		}
	}

	// FUNC_113: Make Toast larger
	private static void customToast(Context context, String content) {
//		LayoutInflater inflater = LayoutInflater.from(context);
//
//		View layout = inflater.inflate(R.layout.toast_layout,
//				(ViewGroup) ((Activity) context)
//						.findViewById(R.id.toast_layout_root));
//
//		TextView text = (TextView) layout.findViewById(R.id.text);
//		text.setText(content);
//
//		Toast toast = new Toast(context);
//		toast.setGravity(Gravity.BOTTOM, 0, 180);
//		toast.setDuration(Toast.LENGTH_SHORT);
//		toast.setView(layout);
//		toast.show();
		Toast.makeText(context, content, Toast.LENGTH_SHORT).show();
	}

	public static void popAlert2Choose(Context context, int title, int message,
			int btnPositiveName, OnClickListener btnPositiveOnClickListener,
			int btnNegativeName, OnClickListener btnNegativeOnClickListener) {
		AlertDialog.Builder alert = new AlertDialog.Builder(context);
		if (title != -1)
			alert.setTitle(title);
		alert.setMessage(message)
				.setPositiveButton(btnPositiveName, btnPositiveOnClickListener)
				.setNegativeButton(btnNegativeName, btnNegativeOnClickListener)
				.create().show();
	}

	public static void popAlert2Choose(Context context, String titleId,
			String message, String btnPositiveName,
			OnClickListener btnPositiveOnClickListener, String btnNegativeName,
			OnClickListener btnNegativeOnClickListener) {
		AlertDialog.Builder alert = new AlertDialog.Builder(context);
		if (!"".equals(titleId))
			alert.setTitle(titleId);
		alert.setMessage(message)
				.setPositiveButton(btnPositiveName, btnPositiveOnClickListener)
				.setNegativeButton(btnNegativeName, btnNegativeOnClickListener)
				.create().show();
	}

	public static void popAlert2Choose(Context context, int title,
			String[] messages, int index, int btnPositiveName,
			OnClickListener btnPositiveOnClickListener, int btnNegativeName,
			OnClickListener btnNegativeOnClickListener) {
		if (messages != null && messages.length > 0) {
			AlertDialog.Builder alert = new AlertDialog.Builder(context);
			alert.setSingleChoiceItems(messages, index, null);
			if (title != -1)
				alert.setTitle(title);
			alert.setPositiveButton(btnPositiveName, btnPositiveOnClickListener)
					.setNegativeButton(btnNegativeName, btnNegativeOnClickListener).create().show();
		}
	}

	public static void popAlert2Choose(Context context, View layout, int title, int message, 
			int btnPositiveName, OnClickListener btnPositiveOnClickListener, 
			int btnNegativeName, OnClickListener btnNegativeOnClickListener) {
		AlertDialog.Builder alert = new AlertDialog.Builder(context);
		if (title != -1) {
			alert.setTitle(title);
		}
		if (message != -1) {
			alert.setMessage(message);
		}
		alert.setCancelable(false);
		alert.setView(layout);
		alert.setPositiveButton(btnPositiveName, btnPositiveOnClickListener);
		if (btnNegativeName != -1) {
			alert.setNegativeButton(btnNegativeName, btnNegativeOnClickListener);
		}
		alert.create().show();
	}

	// ================================
	// Graphics Utilities
	// ================================
	/**
	 * This method return a bitmap from nine path image id with given width and
	 * height
	 */
	public static Bitmap getBitmapFromNinepatch(Context context,
			int drawableId, int w, int h) {
		// id is a resource id for a valid ninepatch
		Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(), drawableId);

		byte[] chunk = bitmap.getNinePatchChunk();
		NinePatchDrawable np_drawable = new NinePatchDrawable(bitmap, chunk, new Rect(), null);
		np_drawable.setBounds(0, 0, w, h);

		Bitmap output_bitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
		Canvas canvas = new Canvas(output_bitmap);
		np_drawable.draw(canvas);

		return output_bitmap;
	}

	public static void startLauncherActivity(Activity act, String packageName, String appName) {
		final Intent intent = new Intent(Intent.ACTION_MAIN, null);
		intent.addCategory(Intent.CATEGORY_LAUNCHER);
		final ComponentName cn = new ComponentName(packageName, appName);
		intent.setComponent(cn);
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		act.startActivity(intent);
	}

	public static boolean setImageView(View iv, Bitmap bitmap) {
		if (bitmap != null) {
			if (iv instanceof ImageView) {
				((ImageView) iv).setImageBitmap(bitmap);
			} else {
				BitmapDrawable drawable = new BitmapDrawable(bitmap);
				iv.setBackgroundDrawable(drawable);
			}
			return true;
		}
		return false;
	}

	@SuppressWarnings("rawtypes")
	public static void unbindDrawables(View view, boolean isRemoveAllViews) {
		if (view != null) {
			if (view.getBackground() != null) {
				view.getBackground().setCallback(null);
			}
			if (view instanceof AdapterView) {
				for (int i = 0; i < ((AdapterView) view).getChildCount(); i++) {
					unbindDrawables(((AdapterView) view).getChildAt(i), isRemoveAllViews);
					View v = ((ViewGroup) view).getChildAt(i);
					if (view instanceof ImageView) {
						((ImageView) v).getDrawingCache().recycle();
					}
				}

			} else if (view instanceof ViewGroup) {
				for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
					unbindDrawables(((ViewGroup) view).getChildAt(i), isRemoveAllViews);
					View v = ((ViewGroup) view).getChildAt(i);
					if (view instanceof ImageView) {
						((ImageView) v).getDrawingCache().recycle();
					}
				}
				if (isRemoveAllViews) {
					((ViewGroup) view).removeAllViews();
				}
			}
		}
	}

	public interface StringRunnable {
		public void run(String string);
	}

	 public static void setListViewHeightBasedOnChildren(ListView listView) {
//	        ListAdapter listAdapter = listView.getAdapter();
//	        if (listAdapter == null) {
//	            // pre-condition
//	            return;
//	        }
//
////	        for (int i = 0; i < listAdapter.getCount(); i++) {
////	            View listItem = listAdapter.getView(i, null, listView);
////	            listItem.measure(0, 0);
////	            totalHeight += listItem.getMeasuredHeight();
////	        }
//
//	        ListAdapter adapter = listView.getAdapter();
//
//	          int listviewHeight = 0;
//
//	          listView.measure(MeasureSpec.makeMeasureSpec(MeasureSpec.UNSPECIFIED, MeasureSpec.UNSPECIFIED),
//	                       MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED));
//	          listviewHeight = listView.getMeasuredHeight() * adapter.getCount() + (adapter.getCount() * listView.getDividerHeight());
//
//
//	        ViewGroup.LayoutParams params = listView.getLayoutParams();
//	        params.height =listviewHeight;
//	        listView.setLayoutParams(params);
//	        listView.requestLayout();

		 ListAdapter listAdapter = listView.getAdapter();
	        if (listAdapter == null) {
	            // pre-condition
	            return;
	        }

	        int totalHeight = 0;
	        int desiredWidth = MeasureSpec.makeMeasureSpec(listView.getWidth(), MeasureSpec.AT_MOST);
	        for (int i = 0; i < listAdapter.getCount(); i++) {
	            View listItem = listAdapter.getView(i, null, listView);
	            listItem.measure(desiredWidth, MeasureSpec.UNSPECIFIED);
	            totalHeight += listItem.getMeasuredHeight();
	        }

	        ViewGroup.LayoutParams params = listView.getLayoutParams();
	        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
	        listView.setLayoutParams(params);
	        listView.requestLayout();
	    }

	public static void setSelectionAtTheEnd(EditText editText) {
		editText.setSelection(editText.getText().length());
	}

	// TODO listener utils

	public static TextWatcher getTextWacher(final Runnable beforeTextChanged,
			final Runnable onTextChanged, final Runnable afterTextChanged) {

		return new TextWatcher() {
		    @Override
			public void afterTextChanged(Editable s){
		    	if (afterTextChanged != null) {
		    		afterTextChanged.run();
		    	}
		    }
		    @Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
		    	if (beforeTextChanged != null) {
		    		beforeTextChanged.run();
		    	}
		    }
		    @Override
			public void onTextChanged(CharSequence s, int start, int before,int count) {
		    	if (onTextChanged != null) {
		    		onTextChanged.run();
		    	}
		    }
		};
	}
	
	// TODO: Convert dp <=> px
	// Convert dp to pixel:
	public static int dpToPx(Context context, int dp) {
		if (context == null)
			return -1;
		
	    DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
	    int px = Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));       
	    return px;
	}
	
	// Convert pixel to dp:
	public static int pxToDp(Context context, int px) {
		if (context == null)
			return -1;
		
	    DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
	    int dp = Math.round(px / (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
	    return dp;
	}

	// Others
	
	@TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
	public static void addEventToCalendar(Context activity, String title,
			Date dateBegin, Date dateEnd) {
		Intent intent = new Intent(Intent.ACTION_EDIT); // ACTION_INSERT
		intent.setData(CalendarContract.Events.CONTENT_URI);
		intent.setType("vnd.android.cursor.item/event");
		if (!Utils.isEmptyString(title))
			intent.putExtra(Events.TITLE, title);

		// For setting dates
		Calendar cal = Calendar.getInstance();
		if (dateBegin != null) {
		    cal.setTime(dateBegin);
			GregorianCalendar gDateBegin = new GregorianCalendar(cal.get(Calendar.YEAR),
					cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH), cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE));
			intent.putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME, gDateBegin.getTimeInMillis());
		}
		if (dateEnd != null) {
			cal.setTime(dateEnd);
			GregorianCalendar gDateEnd = new GregorianCalendar(cal.get(Calendar.YEAR),
					cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH), cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE));
			intent.putExtra(CalendarContract.EXTRA_EVENT_END_TIME, gDateEnd.getTimeInMillis());
		}

//		// Make it a full day event
//		intent.putExtra(CalendarContract.EXTRA_EVENT_ALL_DAY, true);
//		// Make it a recurring Event
//		intent.putExtra(Events.RRULE, "FREQ=WEEKLY;COUNT=11;WKST=SU;BYDAY=TU,TH");
//
//		// Making it private and shown as busy
//		intent.putExtra(Events.ACCESS_LEVEL, Events.ACCESS_PRIVATE);
//		intent.putExtra(Events.AVAILABILITY, Events.AVAILABILITY_BUSY);

		activity.startActivity(intent);
	}

	public static void switchFragment(FragmentActivity context,
			Fragment fragment, int containerId, boolean isAddToBackStack) {
	    if (context != null && fragment != null) {
    		FragmentManager fm = context.getSupportFragmentManager();
    		FragmentTransaction transaction = fm.beginTransaction();
    		transaction.replace(containerId, fragment);
    		if (isAddToBackStack) {
    			transaction.addToBackStack(null);
    		}
    		transaction.commit();
	    }
	}
	
	public static void switchChildFragment(Fragment parentFragment,
            Fragment fragment, int containerId, boolean isAddToBackStack) {
        if (parentFragment != null && fragment != null) {
            FragmentTransaction transaction = parentFragment.getChildFragmentManager().beginTransaction();
            transaction.replace(containerId, fragment, null);
            if (isAddToBackStack) {
                transaction.addToBackStack(null);
            }
            transaction.commit();
        }
    }
	
	public static void switchChildFragment(Fragment parentFragment,
	        Fragment currentFrag, Fragment fragment, int containerId, boolean isAddToBackStack) {
        if (parentFragment != null && fragment != null) {
            if (currentFrag == null) {
                FragmentTransaction transaction = parentFragment.getChildFragmentManager().beginTransaction();
                transaction.replace(containerId, fragment, fragment.getClass().getName());
                if (isAddToBackStack) {
                    transaction.addToBackStack(null);
                }
                transaction.commit();
            } else {
                FragmentManager fm = parentFragment.getChildFragmentManager();
                Fragment f = fm.findFragmentByTag(fragment.getClass().getName());
                if (f != null) {
                    fm.beginTransaction().hide(currentFrag).show(f).commit();
                } else {
                    fm.beginTransaction().hide(currentFrag).add(containerId, fragment, fragment.getClass().getName()).commit();
                }
            }
        }
    }
	
	@SuppressWarnings("deprecation")
    public static BitmapDrawable writeOnDrawable(Context context, int drawableId, String text) {
        Bitmap bm = BitmapFactory.decodeResource(context.getResources(), drawableId).copy(Bitmap.Config.ARGB_8888, true);
        int textSize = (int) context.getResources().getDimension(R.dimen.common_textSize_large);
        Paint paint = new Paint(); 
        paint.setStyle(Style.FILL);  
        paint.setColor(Color.WHITE); 
        paint.setTextSize(textSize); 

        Canvas canvas = new Canvas(bm);
        canvas.drawText(text, 0, textSize, paint);

        return new BitmapDrawable(bm);
    }
	
	@SuppressLint("NewApi")
	@SuppressWarnings("deprecation")
    public static void removeOnGlobalLayoutListener(View v, ViewTreeObserver.OnGlobalLayoutListener listener) {
        if (Build.VERSION.SDK_INT < 16) {
            v.getViewTreeObserver().removeGlobalOnLayoutListener(listener);
        } else {
            v.getViewTreeObserver().removeOnGlobalLayoutListener(listener);
        }
	}

    public static Rect getTextBounder(TextView tv, String text) {
        Paint paint = new Paint();
        Rect bounds = new Rect();

        paint.setTypeface(tv.getTypeface());// your preference here
        paint.setTextSize(tv.getTextSize());// have this the same as your text size
        paint.getTextBounds(text, 0, text.length(), bounds);

        return bounds;
    }

    public static void updateSeekBarText(Context context, TextView tv, String text, int seekbarValue, int totalWidth) {
        tv.setText(text);
        
        float sbLengthUnit = totalWidth / 100;
        float dx = getTextBounder(tv, text).width() / 2;
        float progressTextX = seekbarValue * sbLengthUnit - dx;
//        tv.animate().translationX(progressTextX);
        LayoutParams p = (LayoutParams) tv.getLayoutParams();
        p.leftMargin = (int) progressTextX;
        tv.setLayoutParams(p);
    }

    public static void clearTouchListener(View v) {
        v.setOnTouchListener(new OnTouchListener() {
            
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });
    }

    public static void updatePadding(Context context, CheckBox checkBox) {
        final float scale = context.getResources().getDisplayMetrics().density;
        checkBox.setPadding(checkBox.getPaddingLeft() + (int)(10.0f * scale + 0.5f),
                checkBox.getPaddingTop(),
                checkBox.getPaddingRight(),
                checkBox.getPaddingBottom());
    }
}
