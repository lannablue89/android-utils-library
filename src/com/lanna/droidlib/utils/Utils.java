package com.lanna.droidlib.utils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.pm.Signature;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.provider.Settings.Secure;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.CursorLoader;
import android.util.Base64;
import android.util.Log;

public class Utils {
	public static final String tag = Utils.class.getSimpleName();

	// utility values

	public static final String SIGN_NULL = "null";
	public static final String SIGN_EMPTY = "";
	public static final String SIGN_TRUE = "true";
	public static final String SIGN_FALSE = "false";
	public static final String SIGN_DONE = "done";
	public static final String SIGN_NA = "n/a";
	public static final String SIGN_SEPARATE = "#";

	// utility methods

	public static boolean pushCertificateSSL() {
		TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
			@Override
			public java.security.cert.X509Certificate[] getAcceptedIssuers() {
				return null;
			}

			@Override
			public void checkClientTrusted(java.security.cert.X509Certificate[] certs, String authType) {
			}

			@Override
			public void checkServerTrusted(java.security.cert.X509Certificate[] certs, String authType) {
			}
		} };

		// Install the all-trusting trust manager
		try {
			SSLContext sc = SSLContext.getInstance("SSL");
			// SSLContext sc = SSLContext.getInstance("TLS");
			HostnameVerifier hostnameVerifier = org.apache.http.conn.ssl.SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER;

			sc.init(null, trustAllCerts, new java.security.SecureRandom());
			HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
			HttpsURLConnection.setDefaultHostnameVerifier(hostnameVerifier);
		} catch (Exception e) {
			e.printStackTrace();
			Log.e(tag, "pushCertificate() return false");
			return false;
		}
		return true;
	}

	public static void trustEveryoneTLS() {
		try {
			HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier() {
				@Override
				public boolean verify(String hostname, SSLSession session) {
					return true;
				}
			});
			SSLContext context = SSLContext.getInstance("TLS");
			context.init(null, new X509TrustManager[] { new X509TrustManager() {
				@Override
				public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {
				}

				@Override
				public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
				}

				@Override
				public X509Certificate[] getAcceptedIssuers() {
					return new X509Certificate[0];
				}
			} }, new SecureRandom());
			HttpsURLConnection.setDefaultSSLSocketFactory(context.getSocketFactory());
		} catch (Exception e) {
			// should never happen
			Log.e(tag, "trustEveryoneTLS() error " + e);
		}
	}

	// public DefaultHttpClient getDefaultHttpClient() {
	// SchemeRegistry schemeRegistry = new SchemeRegistry();
	// schemeRegistry.register(new Scheme("http", PlainSocketFactory
	// .getSocketFactory(), 80));
	// schemeRegistry.register(new Scheme("https", new LEasySSLSocketFactory(),
	// 443));
	//
	// HttpParams params = new BasicHttpParams();
	// params.setParameter(ConnManagerPNames.MAX_TOTAL_CONNECTIONS, 30);
	// params.setParameter(ConnManagerPNames.MAX_CONNECTIONS_PER_ROUTE,
	// new ConnPerRouteBean(30));
	// params.setParameter(HttpProtocolParams.USE_EXPECT_CONTINUE, false);
	// HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
	//
	// ClientConnectionManager cm = new SingleClientConnManager(params,
	// schemeRegistry);
	// return new DefaultHttpClient(cm, params);
	// }

	// public static String getMacAddress(Context context) {
	// // Must <uses-permission android:name="android.permission.BLUETOOTH"
	// WifiManager wimanager = (WifiManager) context
	// .getSystemService(Context.WIFI_SERVICE);
	// String address = wimanager.getConnectionInfo().getMacAddress();
	// if (address == null)
	// address = "";
	// LogUtil.e(tag, "MAC address = " + address);
	// return address;
	// }

	// ttnlan update for emulator
	// this require permission "android.permission.READ_PHONE_STATE"
	public static String getUniqueAndroidDeviceId(Context context) {
		return Secure.getString(context.getContentResolver(), Secure.ANDROID_ID);
	}

	public static void changeLanguage(Context context, String language) {
		try {
			Locale locale = new Locale(language);
			Locale.setDefault(locale);
			Configuration config = new Configuration();
			config.locale = locale;
			context.getResources().updateConfiguration(config, context.getResources().getDisplayMetrics());
		} catch (Exception e) {
		}
	}

	// timer
	private static String FORMAT_DAY = "EEE, dd MMM yyyy hh:mm:ss zzz";
	public static DateFormat datef = new SimpleDateFormat(FORMAT_DAY);

	public static Date parsefromString(String date) {
		try {
			return datef.parse(date);
		} catch (Exception e) {
			return null;
		}
	}

	public static long getTimeCurrentCounter(Date date) {
		try {
			long seconds = (System.currentTimeMillis() - date.getTime());
			return seconds;
		} catch (Exception e) {
			Log.e(tag, e.getMessage());
			return 0;
		}
	}

	public static String convertByTimeZone(String timeString) {
		try {
			Date date = new Date(timeString);
			return date.toLocaleString();
		} catch (Exception e) {
			Log.e(tag, "Error! timeString=" + timeString);
		}
		return timeString;
	}

	public static String portTimeToString(long temp) {
		String time;
		temp = temp / 1000;
		int h = (int) temp / 3600;
		int min = (int) temp / 60 - h * 60;
		if (h >= 12)
			h = h - 12;
		time = ((h > 0) ? h + ":" : "00:") + ((min < 10) ? "0" + min : min);
		/* + ":" + ((sec < 10) ? "0" + sec : sec) */
		return time;
	}

	public static String getStringNotNull(String text) {
		return (text == null || SIGN_NULL.endsWith(text)) ? "" : text;
	}

	public static String convertStreamToString(InputStream is) {
		try {
			return new java.util.Scanner(is).useDelimiter("\\A").next();
		} catch (java.util.NoSuchElementException e) {
			return "";
		}
	}

	// getItem(objects, int position)
	public static <T> T getItem(ArrayList<T> models, int position) {
		return isPositionAvailable(models, position) ? models.get(position) : null;
	}
	
	public static <T> T getItem(List<T> models, int position) {
		return isPositionAvailable(models, position) ? models.get(position) : null;
	}

	public static boolean hasData(String string) {
		return string != null && !"".equals(string) && !SIGN_NULL.equals(string);
	}

	public static boolean hasData(List<?> list) {
		return (list != null && !list.isEmpty());
	}

	public static boolean hasData(int[] list) {
		return (list != null && list.length > 0);
	}

	public static boolean hasData(Object[] list) {
		return (list != null && list.length > 0);
	}

	public static boolean hasData(boolean[] list) {
		return (list != null && list.length > 0);
	}

	public static boolean isPositionAvailable(List<?> list, int position) {
		return (list != null && position >= 0 && position < list.size());
	}

	public static <T> T getObject(List<T> list, int position) {
		return isPositionAvailable(list, position) ? list.get(position) : null;
	}

	public static boolean isPositionAvailable(Object[] arrModel, int position) {
		return (arrModel != null && position >= 0 && position < arrModel.length);
	}

	public static boolean isPositionAvailable(int[] array, int position) {
		return (array != null && position >= 0 && position < array.length);
	}
	
	public static int indexOf(List<?> list, Object obj) {
		return list == null ? null : list.indexOf(obj);
	}

	public static String getOrderName(int id) {
		if (id < 10)
			return "00" + id;
		else if (id < 100)
			return "0" + id;
		return "" + id;
	}

	public static String getWhereClauseQueryWithListInteger(String column, ArrayList<Integer> listValues) {
		String whereClause = "(";
		if (listValues.size() == 1) {
			whereClause += column + " = " + listValues.get(0).intValue();
		} else if (listValues.isEmpty()) {
			whereClause += column + " = 0";
		} else {
			for (short i = 0; i < listValues.size(); i++) {
				whereClause += column + " = " + listValues.get(i).intValue();
				if (i != listValues.size() - 1) {
					whereClause += " OR ";
				}
			}
		}
		whereClause += ")";
		return whereClause;
	}

	public static String convertToNonUTF8(String org) {
		// convert to VNese no sign. @haidh 2008
		char arrChar[] = org.toCharArray();
		char result[] = new char[arrChar.length];
		for (int i = 0; i < arrChar.length; i++) {
			switch (arrChar[i]) {
			case '\u00E1':
			case '\u00E0':
			case '\u1EA3':
			case '\u00E3':
			case '\u1EA1':
			case '\u0103':
			case '\u1EAF':
			case '\u1EB1':
			case '\u1EB3':
			case '\u1EB5':
			case '\u1EB7':
			case '\u00E2':
			case '\u1EA5':
			case '\u1EA7':
			case '\u1EA9':
			case '\u1EAB':
			case '\u1EAD':
			case '\u0203':
			case '\u01CE':
				result[i] = 'a';
				break;
			case '\u00E9':
			case '\u00E8':
			case '\u1EBB':
			case '\u1EBD':
			case '\u1EB9':
			case '\u00EA':
			case '\u1EBF':
			case '\u1EC1':
			case '\u1EC3':
			case '\u1EC5':
			case '\u1EC7':
			case '\u0207':
				result[i] = 'e';
				break;
			case '\u00ED':
			case '\u00EC':
			case '\u1EC9':
			case '\u0129':
			case '\u1ECB':
				result[i] = 'i';
				break;
			case '\u00F3':
			case '\u00F2':
			case '\u1ECF':
			case '\u00F5':
			case '\u1ECD':
			case '\u00F4':
			case '\u1ED1':
			case '\u1ED3':
			case '\u1ED5':
			case '\u1ED7':
			case '\u1ED9':
			case '\u01A1':
			case '\u1EDB':
			case '\u1EDD':
			case '\u1EDF':
			case '\u1EE1':
			case '\u1EE3':
			case '\u020F':
				result[i] = 'o';
				break;
			case '\u00FA':
			case '\u00F9':
			case '\u1EE7':
			case '\u0169':
			case '\u1EE5':
			case '\u01B0':
			case '\u1EE9':
			case '\u1EEB':
			case '\u1EED':
			case '\u1EEF':
			case '\u1EF1':
				result[i] = 'u';
				break;
			case '\u00FD':
			case '\u1EF3':
			case '\u1EF7':
			case '\u1EF9':
			case '\u1EF5':
				result[i] = 'y';
				break;
			case '\u0111':
				result[i] = 'd';
				break;
			case '\u00C1':
			case '\u00C0':
			case '\u1EA2':
			case '\u00C3':
			case '\u1EA0':
			case '\u0102':
			case '\u1EAE':
			case '\u1EB0':
			case '\u1EB2':
			case '\u1EB4':
			case '\u1EB6':
			case '\u00C2':
			case '\u1EA4':
			case '\u1EA6':
			case '\u1EA8':
			case '\u1EAA':
			case '\u1EAC':
			case '\u0202':
			case '\u01CD':
				result[i] = 'A';
				break;
			case '\u00C9':
			case '\u00C8':
			case '\u1EBA':
			case '\u1EBC':
			case '\u1EB8':
			case '\u00CA':
			case '\u1EBE':
			case '\u1EC0':
			case '\u1EC2':
			case '\u1EC4':
			case '\u1EC6':
			case '\u0206':
				result[i] = 'E';
				break;
			case '\u00CD':
			case '\u00CC':
			case '\u1EC8':
			case '\u0128':
			case '\u1ECA':
				result[i] = 'I';
				break;
			case '\u00D3':
			case '\u00D2':
			case '\u1ECE':
			case '\u00D5':
			case '\u1ECC':
			case '\u00D4':
			case '\u1ED0':
			case '\u1ED2':
			case '\u1ED4':
			case '\u1ED6':
			case '\u1ED8':
			case '\u01A0':
			case '\u1EDA':
			case '\u1EDC':
			case '\u1EDE':
			case '\u1EE0':
			case '\u1EE2':
			case '\u020E':
				result[i] = 'O';
				break;
			case '\u00DA':
			case '\u00D9':
			case '\u1EE6':
			case '\u0168':
			case '\u1EE4':
			case '\u01AF':
			case '\u1EE8':
			case '\u1EEA':
			case '\u1EEC':
			case '\u1EEE':
			case '\u1EF0':
				result[i] = 'U';
				break;
			case '\u00DD':
			case '\u1EF2':
			case '\u1EF6':
			case '\u1EF8':
			case '\u1EF4':
				result[i] = 'Y';
				break;
			case '\u0110':
			case '\u00D0':
			case '\u0089':
				result[i] = 'D';
				break;
			default:
				result[i] = arrChar[i];
			}
		}
		return new String(result);
	}

	public static <T> String getSizeString(List<T> chairModels) {
		return (chairModels == null) ? "null" : "" + chairModels.size();
	}

	public static boolean createDirIfNotExists(String path) {
		if (!checkFolderExist(path)) {
			File file = new File(path);
			file.mkdir();
			return true;
		}
		return false;
	}

	public static boolean checkFileExist(String path) {
		File file = new File(path);
		if (file.isFile() && file.exists())
			return true;
		return false;
	}

	public static boolean checkFolderExist(String path) {
		File file = new File(path);
		if (file.mkdirs())
			return true;
		return false;
	}

	public static boolean checkFileExistsOnURL(String URL) {
		try {
			HttpURLConnection.setFollowRedirects(false);
			HttpURLConnection con = (HttpURLConnection) new URL(URL).openConnection();
			con.setRequestMethod("HEAD");
			return (con.getResponseCode() == HttpURLConnection.HTTP_OK);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	public static long checkFileLength(String path) {
		File file = new File(path);
		return file.length();
	}

	public static long checkUrlFileLength(String path) {
		try {
			URL url = new URL(path);
			URLConnection urlConnection = url.openConnection();
			urlConnection.connect();
			return urlConnection.getContentLength();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return 0;
	}

	public static boolean deleteFile(String path) {
		File file = new File(path);
		return file.delete();
	}

	public static String removeLetterInString(String name, String letter) {
		if (Utils.hasData(name)) {
			String array[] = name.split(letter);
			if (array.length > 1) {
				name = array[0];
				for (int i = 1; i < array.length; i++)
					name += "_" + array[i];
			}
		}
		return name;
	}

	public static String getDateDMY(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		int year = cal.get(Calendar.YEAR);
		int month = cal.get(Calendar.MONTH) + 1; // Beware, months start at 0,
													// not 1.
		int day = cal.get(Calendar.DAY_OF_MONTH);
		return day + "-" + month + "-" + year; // FIXME 2-6-113

		// String[] formats = new String[] {
		// "yyyy-MM-dd",
		// "yyyy-MM-dd HH:mm",
		// "yyyy-MM-dd HH:mmZ",
		// "yyyy-MM-dd HH:mm:ss.SSSZ",
		// "yyyy-MM-dd'T'HH:mm:ss.SSSZ",
		// };
		// for (String format : formats) {
		// SimpleDateFormat sdf = new SimpleDateFormat(format, Locale.US);
		// System.err.format("%30s %s\n", format, sdf.format(new Date(0)));
		// sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
		// System.err.format("%30s %s\n", format, sdf.format(new Date(0)));
		// }
		//
		// yyyy-MM-dd 1969-12-31
		// yyyy-MM-dd 1970-01-01
		// yyyy-MM-dd HH:mm 1969-12-31 16:00
		// yyyy-MM-dd HH:mm 1970-01-01 00:00
		// yyyy-MM-dd HH:mmZ 1969-12-31 16:00-0800
		// yyyy-MM-dd HH:mmZ 1970-01-01 00:00+0000
		// yyyy-MM-dd HH:mm:ss.SSSZ 1969-12-31 16:00:00.000-0800
		// yyyy-MM-dd HH:mm:ss.SSSZ 1970-01-01 00:00:00.000+0000
		// yyyy-MM-dd'T'HH:mm:ss.SSSZ 1969-12-31T16:00:00.000-0800
		// yyyy-MM-dd'T'HH:mm:ss.SSSZ 1970-01-01T00:00:00.000+0000
	}

	public static Date getDate(String dateTime) {
		String format = "yyyy-MM-dd HH:mm:ss";
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		Date date = null;
		try {
			date = sdf.parse(dateTime);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return date;
	}

	public static boolean validateEmail(String email) {
		return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
	}

	public static void callCamera(Context context) {
		Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
		// String filePath = Environment.getExternalStorageDirectory() +
		// "/avatar.jpeg";
		// File file = new File(filePath);
		// Uri output = Uri.fromFile(file);
		// cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, output);
		((Activity) context).startActivityForResult(cameraIntent, 2);
	}

	public static String getRealPathFromURI(Uri contentUri, Context context) {
		String[] proj = { MediaStore.Images.Media.DATA };
		CursorLoader loader = new CursorLoader(context, contentUri, proj, null, null, null);
		Cursor cursor = loader.loadInBackground();
		int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
		cursor.moveToFirst();
		return cursor.getString(column_index);
	}

	public static String getBase64fromBitmap(Bitmap bm, int scaledW, int scaledH) {
//		bm = Bitmap.createScaledBitmap(bm, scaledW, scaleH, false); // scaledW, scaledH = 200 in old code
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		bm.compress(Bitmap.CompressFormat.PNG, 80, baos); // bm is the bitmap object
		byte[] b = baos.toByteArray();
		String encoded = Base64.encodeToString(b, Base64.DEFAULT);
		System.out.print("base64encoded="+encoded); // to get full bitmap string
		return encoded;

	}

	public static List<ResolveInfo> getCameraApps(Context context) {
    	Intent mIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		return context.getPackageManager().queryIntentActivities(mIntent, 0);
    }

    public static List<ResolveInfo> getGalleryApp(Context context) {
    	Intent mIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
    	return context.getPackageManager().queryIntentActivities(mIntent, 0);
    }

    public static boolean isEmptyString(String str) {
		return str == null || "".equals(str.trim());
	}

    public static int parseInt(String str, int defaultValue) {
		return parseInt(str, defaultValue, false);
	}

	public static int parseInt(String str, int defaultValue, boolean showStackTrace) {
		try {
			return Integer.parseInt(str);
		} catch (Exception e) {
			if (showStackTrace) {
				e.printStackTrace();
			}
		}
		return defaultValue;
	}

	public static float parseFloat(String str, float defaultValue) {
		return parseFloat(str, defaultValue, false);
	}

	public static float parseFloat(String str, float defaultValue, boolean showStackTrace) {
		try {
			return Float.parseFloat(str);
		} catch (Exception e) {
			if (showStackTrace) {
				e.printStackTrace();
			}
		}
		return defaultValue;
	}

	public static Fragment replaceFragment(int placeHolderID, Class<? extends Fragment> fragmentClass, FragmentActivity context) throws Exception {
		FragmentManager fragmentManager = context.getSupportFragmentManager();
		String tag = fragmentClass.getCanonicalName();
		FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
		Fragment fragment = fragmentManager.findFragmentByTag(tag);
		if (fragment != null) {
			fragmentTransaction.remove(fragment);
		}
		fragment = fragmentClass.newInstance();
		fragmentTransaction.replace(placeHolderID, fragment, tag).commit();
		return fragment;
	}

	public static void sendEmail(Context context, String title, String subject
			, String body, String... recipientList) {
        Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);
        emailIntent.setType("plain/text");
        emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, recipientList);
        emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, subject);
        emailIntent.putExtra(android.content.Intent.EXTRA_TEXT, body);
        context.startActivity(Intent.createChooser(emailIntent, title));
	}

	public static void openFacebook(Context context, String fbId, String fbName) {
		try {
			context.getPackageManager().getPackageInfo("com.facebook.katana", 0);
			context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("fb://profile/" + fbId)));
		} catch (Exception e) {
			context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.facebook.com/" + fbName)));
		}
	}

	public static void openTwitter(Context context, String twitterUsername) {
		try {
			context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("twitter://user?screen_name="+twitterUsername)));
		} catch (android.content.ActivityNotFoundException anfe) {
			context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://twitter.com/#!/"+twitterUsername)));
		}
	}

	public static void openGooglePlay(Context context, String appName) {
		try {
			context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id="+appName)));
		} catch (android.content.ActivityNotFoundException anfe) {
			context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id="+appName)));
		}
	}

	public static void openSky(Context context, String nick) {
		try {
			context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("skype:" + nick)));
		} catch (android.content.ActivityNotFoundException anfe) {
			anfe.printStackTrace();
		}
	}

	public static String floatInString(float f) {
	    if (f == (int) f)
	        return String.format("%d", (int) f);
	    else
	        return String.format("%s", f);
	}
    
    public void printHashKey(Context context) {

        try {
            PackageInfo info = context.getPackageManager().getPackageInfo("com.facebook.samples.sessionlogin",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                String key = Base64.encodeToString(md.digest(), Base64.DEFAULT); 
                Log.d("TEMPTAGHASH KEY:", key);
            }
        } catch (NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {

        }
    }
    
    public static String getShortString(String dest, int... length) {
    	if (dest == null)
    		return "null";
    	else { 
    		int subLength = (length != null && length.length >= 1) ? length[0] : 10;
    		if (dest.length() <= subLength)
    			return dest;
    		else 
    			return dest.substring(0, subLength) + ".../" + dest.length() + "c";
    	} 
    }
}