/**
 * 
 */
package com.lanna.droidlib.utils;

import android.graphics.Typeface;
import android.text.TextPaint;
import android.text.style.ClickableSpan;
import android.view.View;

public class ClickSpan extends ClickableSpan {
	
    private OnClickListener mListener;
    private int mCurrentColor = 0;
    private Typeface mTypeface = null;

    public ClickSpan(OnClickListener listener, int color) {
        mListener = listener;
        mCurrentColor = color;
    }
    
    public ClickSpan(OnClickListener listener, int color, Typeface typeface) {
    	this(listener, color);
    	mTypeface = typeface;
    }

    @Override
    public void onClick(View widget) {
//    	Selection.setSelection((Spannable) ((TextView)widget).getText(), 0);
        if (mListener != null)
            mListener.onClick();
    }

    public void updateDrawState(TextPaint ds) {
        super.updateDrawState(ds);
        ds.setUnderlineText(false);
        if (mCurrentColor != -1)
        	ds.setColor(mCurrentColor);
//        ds.bgColor = Color.GRAY;
//        ds.setARGB(255, 255, 255, 255);
        if (mTypeface != null)
        	ds.setTypeface(mTypeface);
        
    }

    public interface OnClickListener {
        void onClick();
    }
}