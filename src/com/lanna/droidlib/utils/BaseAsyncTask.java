package com.lanna.droidlib.utils;

import android.content.Context;
import android.os.AsyncTask;

public class BaseAsyncTask<T1, T2, T3> extends AsyncTask<T1, T2, T3> {
	final String tag = BaseAsyncTask.class.getSimpleName();

	protected Context mContext;
	IProgressDialogHandler mProgressDialogHandler;
	String mMessage; // "...": show, "": show empty, null: not show

	Runnable mRunOnPre;
	Runnable mRunInBackground;
	Runnable mRunOnPos;

	public BaseAsyncTask(Context context) {
		mContext = context;
		mMessage = "";
	}

	public BaseAsyncTask(Context context, int stringId) {
		this(context);
		mMessage = context.getString(stringId);
	}

	public BaseAsyncTask(Context context, 
			Runnable runOnPre, Runnable runInBackground, Runnable runOnPos, 
			String message) {
		this(context, message);
		mRunOnPre = runOnPre;
		mRunInBackground = runInBackground;
		mRunOnPos = runOnPos;
	}

	public BaseAsyncTask(Context context, String message) {
		this(context);
		mMessage = message;
	}

	public BaseAsyncTask(Context context, 
			Runnable runOnPre, Runnable runInBackground, Runnable runOnPos,
			IProgressDialogHandler progressDialog, String message) {
		this(context, runOnPre, runInBackground, runOnPos, message);
		mProgressDialogHandler = progressDialog;
	}


	@Override
	protected void onPreExecute() {
		if (mMessage != null && mProgressDialogHandler != null) {
//			mProgress = ProgressDialog.show(mContext, "", mMessage);
			mProgressDialogHandler.showProgressDialog("", mMessage);
		}
		if (mRunOnPre != null) {
			mRunOnPre.run();
		}
	}

	@Override
	protected T3 doInBackground(T1... params) {
		if (mRunInBackground != null) {
			mRunInBackground.run();
		}
		return null;
	}

	@Override
	protected void onPostExecute(T3 result) {
		if (mProgressDialogHandler != null) {
			mProgressDialogHandler.dismissProgressDialog();
		}
		if (mRunOnPos != null) {
			mRunOnPos.run();
		}
	}
}