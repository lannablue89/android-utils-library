package com.lanna.droidlib.utils;

public interface IProgressDialogHandler {

	public void showProgressDialog();
	public void showProgressDialog(String title, String message);
	public void dismissProgressDialog();

}