package com.lanna.droidlib.utils;

import java.util.Locale;

import android.content.Context;
import android.content.SharedPreferences;

public class PreferrenceUtils {

	public static final String DEFAULT_LANGUAGE = Locale.ENGLISH.getLanguage();

	public static final String DEFAULT_STRING = "";
	public static final boolean DEFAULT_BOOLEAN = false;
	public static final int DEFAULT_NUMBER = 0;
	
	public static final String mPreferenceName = "references";
	public static final int DEFAULT_REFER_MODE = Context.MODE_PRIVATE;


	private static SharedPreferences mSharedPreferences = null;
	private static SharedPreferences getReferrence(Context context) {
		if (mSharedPreferences == null)
			mSharedPreferences = context.getSharedPreferences(
				mPreferenceName, DEFAULT_REFER_MODE);
		return mSharedPreferences;
	}

	// get, set string
	protected static void saveString(Context context, String key, String value) {
		SharedPreferences.Editor editor = getReferrence(context).edit();
		editor.putString(key, value);
		editor.commit();
	}

	protected static String loadString(Context context, String key, String backup) {
		String result = getReferrence(context).getString(key, backup);
		return result;
	}

	// get, set integer
	protected static void saveInt(Context context, String key, int value) {
		SharedPreferences.Editor editor = getReferrence(context).edit();
		editor.putInt(key, value);
		editor.commit();
	}

	protected static int loadInt(Context context, String key, int backup) {
		int result = getReferrence(context).getInt(key, backup);
		return result;
	}

	// get, set long
	protected static void saveLong(Context context, String key, long value) {
		SharedPreferences.Editor editor =  getReferrence(context).edit();
		editor.putLong(key, value);
		editor.commit();
	}

	protected static long loadLong(Context context, String key, long backup) {
		long result = getReferrence(context).getLong(key, backup);
		return result;
	}

	// get, set float
	protected static void saveFloat(Context context, String key, float value) {
		SharedPreferences.Editor editor = getReferrence(context).edit();
		editor.putFloat(key, value);
		editor.commit();
	}

	protected static float loadFloat(Context context, String key, float backup) {
		float result = getReferrence(context).getFloat(key, backup);
		return result;
	}

	// get, set boolean
	protected static void saveBool(Context context, String key, boolean value) {
		SharedPreferences.Editor editor = getReferrence(context).edit();
		editor.putBoolean(key, value);
		editor.commit();
	}

	protected static boolean loadBool(Context context, String key, boolean backup) {
		boolean result = getReferrence(context).getBoolean(key, backup);
		return result;
	}
}
