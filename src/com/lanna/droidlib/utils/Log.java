package com.lanna.droidlib.utils;

import java.util.ArrayList;
import java.util.List;

public class Log {

	protected static boolean isDebug = true;
	protected static String tag = "util-lib";

	// TODO Log model interface
	public interface ILogModel {
		public String toLogString();
	}

	public static String toLogStrings(ArrayList<? extends ILogModel> logModels) {
		if (logModels != null) {
			String log = "[";
			ILogModel model = null;
			int n = logModels.size() - 1;
			for (int i = 0; i <= n; i++) {
				model = logModels.get(i);
				log += model.toLogString();
				if (i != n) {
					log += "\n";
				}
			}
			log += "]";
			return log;
		}
		return "null";
	}

	public static <T> void printArrayLog(String tag, List<T> models) {
		android.util.Log.i(tag, "[");
		for (T model : models) {
			android.util.Log.i(tag, ""+models.indexOf(model)+": " + model);
		}
		android.util.Log.i(tag, "]");
	}

	// TODO Log(tag, msg)
	public static void d(String tag, String msg) {
		if (isDebug) {
			android.util.Log.d(tag, getMessageNotNull(msg));
		}
	}

	public static void w(String tag, String msg) {
		if (isDebug) {
			android.util.Log.w(tag, getMessageNotNull(msg));
		}
	}

	public static void i(String tag, String msg) {
		if (isDebug) {
			android.util.Log.i(tag, getMessageNotNull(msg));
		}
	}

	public static void v(String tag, String msg) {
		if (isDebug) {
			android.util.Log.v(tag, getMessageNotNull(msg));
		}
	}

	public static void e(String tag, String msg) {
		if (isDebug) {
			android.util.Log.e(tag, getMessageNotNull(msg));
		}
	}

	// TODO Log(tag, msg, Throwable)
	public static void d(String tag, String msg, Throwable e) {
		if (isDebug) {
			android.util.Log.d(tag, getMessageNotNull(msg), e);
		}
	}

	public static void w(String tag, String msg, Throwable e) {
		if (isDebug) {
			android.util.Log.w(tag, getMessageNotNull(msg), e);
		}
	}

	public static void i(String tag, String msg, Throwable e) {
		if (isDebug) {
			android.util.Log.i(tag, getMessageNotNull(msg), e);
		}
	}

	public static void v(String tag, String msg, Throwable e) {
		if (isDebug) {
			android.util.Log.v(tag, getMessageNotNull(msg), e);
		}
	}

	public static void e(String tag, String msg, Throwable e) {
		if (isDebug) {
			android.util.Log.e(tag, getMessageNotNull(msg), e);
		}
	}

	// Add methods for object
	// TODO Log(Object, msg)
	public static void e(Object o, String msg) {
		if (isDebug) {
			android.util.Log.e(o.getClass().getSimpleName(), getMessageNotNull(msg));
		}
	}

	public static void d(Object o, String msg) {
		if (isDebug) {
			android.util.Log.d(o.getClass().getSimpleName(), getMessageNotNull(msg));
		}
	}

	public static void w(Object o, String msg) {
		if (isDebug) {
			android.util.Log.w(o.getClass().getSimpleName(), getMessageNotNull(msg));
		}
	}

	public static void i(Object o, String msg) {
		if (isDebug) {
			android.util.Log.i(o.getClass().getSimpleName(), getMessageNotNull(msg));
		}
	}

	public static void v(Object o, String msg) {
		if (isDebug) {
			android.util.Log.v(o.getClass().getSimpleName(), getMessageNotNull(msg));
		}
	}

	// TODO Log(Object, msg, Throwable)
	public static void e(Object o, String msg, Throwable e) {
		if (isDebug) {
			android.util.Log.e(o.getClass().getSimpleName(), getMessageNotNull(msg), e);
		}
	}

	public static void d(Object o, String msg, Throwable e) {
		if (isDebug) {
			android.util.Log.d(o.getClass().getSimpleName(), getMessageNotNull(msg), e);
		}
	}

	public static void w(Object o, String msg, Throwable e) {
		if (isDebug) {
			android.util.Log.w(o.getClass().getSimpleName(), getMessageNotNull(msg), e);
		}
	}

	public static void i(Object o, String msg, Throwable e) {
		if (isDebug) {
			android.util.Log.i(o.getClass().getSimpleName(), getMessageNotNull(msg), e);
		}
	}

	public static void v(Object o, String msg, Throwable e) {
		if (isDebug) {
			android.util.Log.v(o.getClass().getSimpleName(), getMessageNotNull(msg), e);
		}
	}

	// TODO Log(func, tag, msg) (add 12/25/2012 by ttnlan)
	public static void d(String func, String tag, String msg) {
		if (isDebug) {
			android.util.Log.d(func, tag+": "+getMessageNotNull(msg));
		}
	}

	public static void w(String func, String tag, String msg) {
		if (isDebug) {
			android.util.Log.w(func, tag+": "+getMessageNotNull(msg));
		}
	}

	public static void i(String func, String tag, String msg) {
		if (isDebug) {
			android.util.Log.i(func, tag+": "+getMessageNotNull(msg));
		}
	}

	public static void v(String func, String tag, String msg) {
		if (isDebug) {
			android.util.Log.v(func, tag+": "+getMessageNotNull(msg));
		}
	}

	public static void e(String func, String tag, String msg) {
		if (isDebug) {
			android.util.Log.e(func, tag+": "+getMessageNotNull(msg));
		}
	}

	// TODO Log(func, Object, msg) (add 12/25/2012 by ttnlan)
	public static void e(String func, Object o, String msg) {
		if (isDebug) {
			android.util.Log.e(func, o.getClass().getSimpleName() + ": " + getMessageNotNull(msg));
		}
	}

	public static void d(String func, Object o, String msg) {
		if (isDebug) {
			android.util.Log.d(func, o.getClass().getSimpleName() + ": " + getMessageNotNull(msg));
		}
	}

	public static void w(String func, Object o, String msg) {
		if (isDebug) {
			android.util.Log.w(func, o.getClass().getSimpleName() + ": " + getMessageNotNull(msg));
		}
	}

	public static void i(String func, Object o, String msg) {
		if (isDebug) {
			android.util.Log.i(func, o.getClass().getSimpleName() + ": " + getMessageNotNull(msg));
		}
	}

	public static void v(String func, Object o, String msg) {
		if (isDebug) {
			android.util.Log.v(func, o.getClass().getSimpleName() + ": " + getMessageNotNull(msg));
		}
	}

	// TODO Log(msg) (add 11/29/2013 by ttnlan)
	public static void v(String msg) {
		v(tag, msg);
	}
	
	public static void d(String msg) {
		d(tag, msg);
	}
	
	public static void i(String msg) {
		i(tag, msg);
	}
	
	public static void w(String msg) {
		w(tag, msg);
	}
	
	public static void e(String msg) {
		e(tag, msg);
	}

	// Other support
	private static String getMessageNotNull(String msg) {
		if (msg == null) {
			msg = "null";
		}
		return msg;
	}
}
