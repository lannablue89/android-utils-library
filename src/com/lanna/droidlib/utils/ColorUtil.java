package com.lanna.droidlib.utils;

public class ColorUtil {

	
	public static String availableParseColorString(String color) {
		if (color == null || color.length() == 0) {
			color = "#000000";
		}
		
		if (!color.startsWith("#")) {
			color = "#" + color;
		}
		
		return color.toUpperCase();
	}

}
