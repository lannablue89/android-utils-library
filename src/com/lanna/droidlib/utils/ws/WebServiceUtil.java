package com.lanna.droidlib.utils.ws;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.Charset;

import org.json.JSONObject;

import com.google.gson.Gson;
import com.lanna.droidlib.utils.Log;

public class WebServiceUtil {
	public static final String tag = WebServiceUtil.class.getSimpleName();

	protected static final int TIMEOUT_CONNECTION = 10000;
	
	protected static <T> T requestModelByClass(String url, Class<T> classOfT) throws Exception {
		Log.w(tag, "url=" + url);
		String response = getData(url);
		return parseModelByClass(response, classOfT);
	}
	
	public static <T> T parseModelByClass(String response, Class<T> classOfT) throws Exception {
		Log.i(tag, "response=" + response);
		T result = null;
		if (response != null) {
			Gson gson = new Gson();
			result = gson.fromJson(response, classOfT);
			gson = null;
		}
		return result;
	}
	
	protected static <T> T requestModelByType(String url, Type type) throws Exception {
		Log.w(tag, "url=" + url);
		String response = getData(url);
		return parseModelsByType(response, type);
	}

	protected static <T> T parseModelsByType(String response, Type type) throws Exception {
		T result = null;
		if (response != null) {
			Gson gson = new Gson();
			result = gson.fromJson(response, type);
			gson = null;
		}
		return result;
	}

	protected static String postJson(String link, JSONObject jo) throws Exception {
		URL url = new URL(link);
		URLConnection conn = url.openConnection();
		conn.setConnectTimeout(TIMEOUT_CONNECTION);
		conn.setReadTimeout(TIMEOUT_CONNECTION);
		((HttpURLConnection) conn).setRequestMethod("POST");
		conn.setDoOutput(true);
		conn.setDoInput(true);
		conn.setRequestProperty("Content-Type", "application/json");
		conn.setRequestProperty("Content-Length", "" + jo.toString().length());

		OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
		wr.write(jo.toString());
		wr.flush();

		// Get the response
		BufferedReader in = new BufferedReader(new InputStreamReader(
				conn.getInputStream(), Charset.forName("UTF-8")));
		String inputLine;

		String result = "";
		while ((inputLine = in.readLine()) != null) {
			result += inputLine;
		}
		return result;
	}

	protected static String getData(String url) throws Exception {
		String response = null;
		URL myUrl = new URL(url);
		HttpURLConnection con = (HttpURLConnection) myUrl.openConnection();
		con.setConnectTimeout(TIMEOUT_CONNECTION);
		con.setReadTimeout(TIMEOUT_CONNECTION);
		InputStream ins = con.getInputStream();
		InputStreamReader isr = new InputStreamReader(ins);
		BufferedReader in = new BufferedReader(isr);
		String inputLine;
		String temp = "";
		while ((inputLine = in.readLine()) != null) {
			temp += inputLine;
		}
		in.close();
		response = temp;
		return response;
	}
	
	// TODO other
	public static String getHttpUrl(String fileURL) {
		fileURL = fileURL.replaceAll(" ", "%20");
		return fileURL;
	}
}
